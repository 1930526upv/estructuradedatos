public class IntSLLNodo {
    public int info;//Tipo de dato a almacenar en nodo
    public IntSLLNodo next;//Direccion de memoria siguiente nodo

    public IntSLLNodo(int i){
        this(i, null);
    }

    public IntSLLNodo(int i, IntSLLNodo n){
        info = i;
        next = n;
    }
}
