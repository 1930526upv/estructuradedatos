public class TestSLList {
    public static void main (String args[]){
        IntSLList lista;
        lista = new IntSLList();
        
        System.out.println(lista.isEmpty() );
        
        lista.addToHead(8);
        lista.printAll();
        System.out.println();
        lista.addToHead(3);
        lista.printAll();
        System.out.println();
        lista.addToTail(2);
        lista.printAll();
        System.out.println();
        lista.delete(8);
        lista.printAll();
        System.out.println();
         
        System.out.println(lista.isEmpty() );
    }
}