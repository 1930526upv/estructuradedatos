public class IntSLList {
    protected IntSLLNodo head;
    protected IntSLLNodo tail;

    public IntSLList(){
        head = tail = null;
    }

    public boolean isEmpty(){
        return head == null;
    }

    public void addToHead(int el){
        head = new IntSLLNodo(el, head);
        if(tail == null)
            tail = head;
    }

    public void addToTail(int el){
        if( !isEmpty() ){
            tail.next = new IntSLLNodo(el);
            tail = tail.next;
        }
        else{
            head = tail = new IntSLLNodo(el);
        }
    }
    
    public int deleteFromHead(){
        int el = tail.info;
        if( head == tail )//Solo hay un nodo en la SLL
            head = tail = null;
        else//si hay mas de un nodo
            head = head.next;
        return el;

    }

    public int deleteFromTail(){
        int el = tail.info;
        if( head == tail )//Solo hay un nodo en la SLL
            head = tail = null;
        else{//si hay mas de un nodo
            IntSLLNodo tmp;
            for( tmp = head; tmp.next != tail; tmp = tmp.next );
            tail = tmp;
            tail.next = null;
        }
        return el;
    }

    public void printAll(){
        for( IntSLLNodo tmp = head; tmp != null; tmp = tmp.next )
            System.out.print( tmp.info + "   " );
    }

    public boolean isInList( int el ){
        IntSLLNodo tmp;
        for(tmp = head; tmp != null && tmp.info != el; tmp = tmp.next );
        return tmp != null;
    }

    public void delete(int el){//Find first node "el" and delete it
        if( !isEmpty() )
            if( head == tail && head.info == el )
                head = tail = null;
            else if( el == head.info )//Si hay mas de un nodo en SLL
                head = head.next;//Se borro el nodo
        else{
            IntSLLNodo pred, tmp;//Si el elemento no esta en el nodo HEAD
            for(
                pred = head, tmp = head.next;
                tmp != null && tmp.info != el;
                pred = pred.next, tmp = tmp.next
            );
            if( tmp != null ){//el elemento se encontro
                pred.next = tmp.next;
                if( tmp == tail )//si el elemento se hallo en tail
                    tail = pred;
            }

        }
    }
}