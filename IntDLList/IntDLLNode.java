public class IntDLLNode {
    public int info;
    public IntDLLNode prev;
    public IntDLLNode next;

    public IntDLLNode(int el){
        this(el, null, null);
    }

    public IntDLLNode(int el, IntDLLNode p, IntDLLNode n){
        info = el;
        prev = p;
        next = n;
    }
}
