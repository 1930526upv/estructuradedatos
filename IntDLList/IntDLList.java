public class IntDLList {
    private IntDLLNode head, tail;

    public IntDLList(){
        head = tail = null;
    }

    public boolean isEmpty(){
        return head == null;
    }

    public void addToTail(int el){
        if(!isEmpty()){
            tail = new IntDLLNode(el, null, tail);
            tail.prev.next = tail;
        }
        else{
            head = tail = new IntDLLNode(el);
        }
    }
}
