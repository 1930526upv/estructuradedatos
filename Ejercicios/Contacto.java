import java.io.Serializable;
import java.util.Date;
import java.io.Serializable;

public class Contacto extends Persona implements Serializable{
    String email;
    long telefono;
    int ID; //CURP, INE, NSS

    public Contacto(String a, Date b){
        super(a, b);
    }

    public Contacto(String a, Date b, String c, long d){
        super(a, b);
        this.email = c;
        this.telefono = d;
    }

    public long getTelefono(){
        return this.telefono;
    }
    public void setTelefono(long a){
        this.telefono = a;
    }

    public String getEmail(){
        return this.email;
    }
    public void setEmail(String b){
        this.email = b;
    }
   /* 
    public Contacto(String nombre, int telefono, int ID){
        this.nombre = nombre;
        this.telefono = telefono;
        this.ID = ID;
    }
    public Contacto(String nombre, int telefono, int ID, String email, String direccion, Date dob){
        this(nombre, telefono, ID);
        this.email = email;
        this.direccion = direccion;
        this.dob = dob;
    }*/
    //Add setters and getters
    public String toString(){
        return ID + '\n' + nombre + '\n' + email + '\n' + telefono + '\n' + fdn.toString() + '\n';
    }

    public boolean equals(Contacto obj){
        return this.telefono == obj.telefono;
    }
}
