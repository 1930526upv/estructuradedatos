import java.util.*;

public class Persona {
    String nombre;
    Date fdn;

    public Persona(String a, Date b){
        this.nombre = a;
        this.fdn = b;
    }

    public String getNombre(){
        return this.nombre;
    }

    public void setNombre(String a){
        this.nombre = a;
    }

    public Date getFdn(){
        return this.fdn;
    }

    public void setFdn(Date b){
        this.fdn = b;
    }
    
    public boolean equals(Persona a){
        return this.nombre.equals(a.nombre) && this.fdn.equals(a.fdn);
    }

    public String toString(){
        return this.nombre.toString() + ' ' + this.fdn.toString();
    }
}
