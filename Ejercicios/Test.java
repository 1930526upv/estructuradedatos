import java.util.*;
import java.io.*;

public class Test {
    public static void main(String[] args) {
        String nombre;
        String email;
        long telefono;
        Date fdn = new Date();
        nombre = "Beto de Plaza Sesamo";
        email = "beto@upv.edu.mx";
        telefono = 8341567800L;

        Contacto a = new Contacto(nombre, fdn, email, telefono);
        System.out.println(a.toString());

        try{
            FileOutputStream fos = new FileOutputStream("Data.ser");
            ObjectOutputStream out = new ObjectOutputStream(fos);

            out.writeObject(a);
            out.close();
            fos.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
